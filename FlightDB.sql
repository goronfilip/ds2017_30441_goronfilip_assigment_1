-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema flights
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema flights
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `flights` DEFAULT CHARACTER SET utf8 ;
USE `flights` ;

-- -----------------------------------------------------
-- Table `flights`.`flights`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `flights`.`flights` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `flightNumber` INT(11) NOT NULL,
  `airplaneType` VARCHAR(45) NOT NULL,
  `fromLocation` VARCHAR(45) NOT NULL,
  `fromTime` VARCHAR(45) NOT NULL,
  `fromCoords` VARCHAR(45) NOT NULL,
  `toLocation` VARCHAR(45) NOT NULL,
  `toTime` VARCHAR(45) NOT NULL,
  `toCoords` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `flights`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `flights`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `userName` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `type` VARCHAR(45) NOT NULL DEFAULT 'user',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `userName_UNIQUE` (`userName` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Default data
-- -----------------------------------------------------
INSERT 
	INTO users (userName, password, type)
	VALUES ('admin', 'admin', 'admin');

INSERT 
	INTO users (userName, password, type)
	VALUES ('user', 'user', 'user');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
