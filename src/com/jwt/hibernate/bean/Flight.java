package com.jwt.hibernate.bean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Flight {
	private int id;
	private int flightNumber;
	private String airplaneType;
	private String from;
	private String fromTime;
	private String fromCoords;
	private String to;
	private String toTime;
	private String toCoords;

	public Flight(int id, int flightNumber, String airplaneType, String from, String fromTime, String fromCoords,
			String to, String toTime, String toCoords) {
		super();
		this.id = id;
		this.flightNumber = flightNumber;
		this.airplaneType = airplaneType;
		this.from = from;
		this.fromTime = fromTime;
		this.fromCoords = fromCoords;
		this.to = to;
		this.toTime = toTime;
		this.toCoords = toCoords;
	}

	public Flight() {
		super();
	}

	public Flight(int id) {
		super();
		this.id = id;
	}

	@Override
	public String toString() {
		return "FlightNumber=" + flightNumber + ", airplaneType=" + airplaneType + ", from=" + from + ", fromTime="
				+ fromTime + ", to=" + to + ", toTime=" + toTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getAirplaneType() {
		return airplaneType;
	}

	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getFromTime() {
		return fromTime;
	}

	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}

	public String getFromCoords() {
		return fromCoords;
	}

	public void setFromCoords(String fromCoords) {
		this.fromCoords = fromCoords;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getToTime() {
		return toTime;
	}

	public void setToTime(String toTime) {
		this.toTime = toTime;
	}

	public String getToCoords() {
		return toCoords;
	}

	public void setToCoords(String toCoords) {
		this.toCoords = toCoords;
	}

	public boolean validate() {
		try {
			if (!Integer.toString(flightNumber).matches("[0-9]+"))
				return false;
			String[] timeDate = fromTime.split(";");
			if (fromTime == null || !timeDate[0].matches("([01]?[0-9]|2[0-3]):[0-5][0-9]"))
				return false;
			if (!isThisDateValid(timeDate[1], "dd/MM/yyyy"))
				return false;
			if (fromCoords == null || !fromCoords.matches("([+-]?\\d+\\.?\\d+)/([+-]?\\d+\\.?\\d+)"))
				return false;
			timeDate = toTime.split(";");
			if (toTime == null || !timeDate[0].matches("([01]?[0-9]|2[0-3]):[0-5][0-9]"))
				return false;
			if (!isThisDateValid(timeDate[1], "dd/MM/yyyy"))
				return false;
			if (toCoords == null || !toCoords.matches("([+-]?\\d+\\.?\\d+)/([+-]?\\d+\\.?\\d+)"))
				return false;
			if (to == null || from == null || airplaneType == null)
				return false;
		} catch (Exception e) {
			return false;
		}

		return true;
	}

	public boolean isThisDateValid(String dateToValidate, String dateFromat) {
		if (dateToValidate == null)
			return false;
		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);
		try {
			Date date = sdf.parse(dateToValidate);
			System.out.println(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
