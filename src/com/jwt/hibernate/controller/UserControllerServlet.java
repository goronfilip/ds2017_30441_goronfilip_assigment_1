package com.jwt.hibernate.controller;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.jwt.hibernate.pages.AdminPage;
import com.jwt.hibernate.pages.Login;
import com.jwt.hibernate.bean.Flight;
import com.jwt.hibernate.dao.FlightDAO;
import com.jwt.hibernate.dao.UserDAO;
import com.jwt.hibernate.webservice.WeatherService;

public class UserControllerServlet extends HttpServlet implements Servlet, Filter {

	private static final long serialVersionUID = 1L;

	public static UserDAO UserDao = new UserDAO();
	public static FlightDAO FlightDao = new FlightDAO();
	public static WeatherService Time = new WeatherService();

	public HttpSession session;

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;

		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("username") == null) {
			response.sendRedirect("welcome.html"); // No logged-in user found, so redirect to login page.
		} else {
			response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
			response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
			response.setDateHeader("Expires", 0);
			chain.doFilter(req, res);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getParameter("action");

		System.out.println(action);

		if (action.equals("login")) {
			Login.doLogin(request, response);
		} else if (action.contains("saveNewPlane")) {
			if (action.contains("edit"))
				FlightDao.updateFlight(request, response);
			else
				FlightDao.createFlight(request, response);
		} else if (action.equals("newPlane")) {
			response.getWriter().print(AdminPage.getEditPage(new Flight(0, 0, "", "", "", "", "", "", ""), "", ""));
		} else if (action.contains("deleteFlight")) {
			FlightDao.deleteFlight(request, response);
		} else if (action.contains("editFlight")) {
			FlightDao.editFlight(request, response);
		} else if (action.equals("goLogin")) {
			RequestDispatcher reponse = request.getRequestDispatcher("Login.html");
			reponse.forward(request, response);
		}

	}

	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}
}