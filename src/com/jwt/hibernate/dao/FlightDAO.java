package com.jwt.hibernate.dao;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.jwt.hibernate.bean.Flight;
import com.jwt.hibernate.pages.AdminPage;

public class FlightDAO {

	public void createFlight(HttpServletRequest request, HttpServletResponse response) {

		try {
			Configuration configuration = new Configuration().configure();
			SessionFactory sessionFactory = configuration.buildSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();

			int flightNumber = Integer.parseInt(request.getParameter("flightNumber"));
			String airplaneType = request.getParameter("airplaneType");
			String from = request.getParameter("from");
			String fromTime = request.getParameter("fromTime");
			String fromCoords = request.getParameter("fromCoords");
			String to = request.getParameter("to");
			String toTime = request.getParameter("toTime");
			String toCoords = request.getParameter("toCoords");

			Flight s = new Flight(0, flightNumber, airplaneType, from, fromTime, fromCoords, to, toTime, toCoords);

			if (!s.validate()) {
				response.getWriter().print(AdminPage.getEditPage(s, "", "Invalid input"));
				return;
			}

			System.out.println(s.toString());
			session.save(s);
			transaction.commit();

			response.getWriter().print(AdminPage.getAdminPage());

		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("error");
		}

	}

	@SuppressWarnings("unchecked")
	public List<Flight> getFlights() {
		try {
			Configuration configuration = new Configuration().configure();
			SessionFactory sessionFactory = configuration.buildSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();

			List<Flight> f = session.createQuery("FROM " + Flight.class.getName()).list();

			transaction.commit();

			System.out.println(f.get(0).toString());

			return f;

		} catch (HibernateException e) {
			System.out.println(e.getMessage());
			System.out.println("error");
		}

		return null;
	}

	public void deleteFlight(HttpServletRequest request, HttpServletResponse response) {
		try {
			Configuration configuration = new Configuration().configure();
			SessionFactory sessionFactory = configuration.buildSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();

			String idString = request.getParameter("action").split(":")[1];
			int id = Integer.parseInt(idString);

			session.delete(new Flight(id));

			transaction.commit();

			response.getWriter().print(AdminPage.getAdminPage());

		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("error");
		}

	}

	@SuppressWarnings("unchecked")
	public void editFlight(HttpServletRequest request, HttpServletResponse response) {
		try {
			Configuration configuration = new Configuration().configure();
			SessionFactory sessionFactory = configuration.buildSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();

			String idString = request.getParameter("action").split(":")[1];
			int id = Integer.parseInt(idString);

			List<Flight> fs = session.createQuery("FROM " + Flight.class.getName() + " WHERE id=" + id).list();
			Flight f = fs.get(0);

			transaction.commit();

			response.getWriter().print(AdminPage.getEditPage(f, "edit", ""));

		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("error");
		}
	}

	public void updateFlight(HttpServletRequest request, HttpServletResponse response) {
		try {
			Configuration configuration = new Configuration().configure();
			SessionFactory sessionFactory = configuration.buildSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();

			System.out.println(request.getParameter("action"));

			String idString = request.getParameter("action").split(":")[2];
			int id = Integer.parseInt(idString);
			int flightNumber = Integer.parseInt(request.getParameter("flightNumber"));
			String airplaneType = request.getParameter("airplaneType");
			String from = request.getParameter("from");
			String fromTime = request.getParameter("fromTime");
			String fromCoords = request.getParameter("fromCoords");
			String to = request.getParameter("to");
			String toTime = request.getParameter("toTime");
			String toCoords = request.getParameter("toCoords");

			Flight s = new Flight(id, flightNumber, airplaneType, from, fromTime, fromCoords, to, toTime, toCoords);

			if (!s.validate()) {
				response.getWriter().print(AdminPage.getEditPage(s, "edit", "Invalid input"));
				return;
			}

			session.update(s);

			transaction.commit();

			response.getWriter().print(AdminPage.getAdminPage());

		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("error");
		}
	}

}
