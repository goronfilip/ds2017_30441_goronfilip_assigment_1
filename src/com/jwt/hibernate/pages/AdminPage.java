package com.jwt.hibernate.pages;

import java.util.List;

import com.jwt.hibernate.bean.Flight;
import com.jwt.hibernate.controller.UserControllerServlet;

public class AdminPage {

	public static String getAdminPage() {

		String page = "";

		page += "<!DOCTYPE html><html><head><meta charset=UTF-8''><title>Admin</title></head><body>";

		page += "<form method=\"post\" action=\"server\">\r\n"
				+ "		<input type=\"hidden\" name=\"action\" value='newPlane'/>\r\n"
				+ "		<div><input value=\"New Flight\" type=\"submit\" /></div>\r\n" + "</form>";

		try {
			List<Flight> flights = UserControllerServlet.FlightDao.getFlights();
			if (!flights.isEmpty()) {
				for (Flight f : flights) {
					page += "<div><form method='post' action='server'>" + f.toString()
							+ "<input type='hidden' name='action' value='deleteFlight:" + f.getId()
							+ "'/><input value='Delete' type='submit' /></form>"
							+ "<form method='post' action='server'><input type='hidden' name='action' value='editFlight:"
							+ f.getId() + "'><input value='Edit' type='submit' /></form></div><br><br>";
				}
			}
		} catch (Exception e) {
		}
		;
		System.out.println("Start Page");
		page += "</body></html>";

		return page;
	}

	public static String getEditPage(Flight flight, String edit, String message) {

		String flightNumber = Integer.toString(flight.getFlightNumber());
		String airplaneType = flight.getAirplaneType();
		String from = flight.getFrom();
		String fromTime = flight.getFromTime();
		String fromCoords = flight.getFromCoords();
		String to = flight.getTo();
		String toTime = flight.getToTime();
		String toCoords = flight.getToCoords();

		String page = "<!DOCTYPE html>\r\n" + "<html>\r\n" + "<body>\r\n" + "<font color=\"red\">" + message
				+ "</font><br>" + "	<form method='post' action='server'>\r\n" + "		<div>\r\n"
				+ "			Flight Number :\r\n" + "			<div></div>\r\n"
				+ "			<input type='text' name='flightNumber' value='" + flightNumber + "'/>\r\n"
				+ "		</div>\r\n" + "		<div>\r\n" + "			Airplane Type :\r\n" + "			<div></div>\r\n"
				+ "			<input type='text' name='airplaneType' value='" + airplaneType + "'/>\r\n"
				+ "		</div>\r\n" + "		<div>\r\n" + "			Departure City :\r\n" + "			<div></div>\r\n"
				+ "			<input type='text' name='from' value='" + from + "'/>\r\n" + "		</div>\r\n"
				+ "		<div>\r\n" + "			Departure Time :\r\n" + "			<div></div>\r\n"
				+ "			<input type='text' name='fromTime' value='" + fromTime + "'/>HH:MM;DD/MM/YYYY ex.\r\n"
				+ "			18:45;31/12/2017\r\n" + "		</div>\r\n" + "		<div>\r\n"
				+ "			Departure Coordinates :\r\n" + "			<div></div>\r\n"
				+ "			<input type='text' name='fromCoords' value='" + fromCoords + "'/>Lat/Long ex.\r\n"
				+ "			18.453/35.677\r\n" + "		</div>\r\n" + "		<div>\r\n" + "			Arrival City :\r\n"
				+ "			<div></div>\r\n" + "			<input type='text' name='to' value='" + to + "'/>\r\n"
				+ "		</div>\r\n" + "		<div>\r\n" + "			Arrival Time :\r\n" + "			<div></div>\r\n"
				+ "			<input type='text' name='toTime' value='" + toTime + "'/>HH:MM;DD/MM/YYYY\r\n"
				+ "		</div>\r\n" + "		<div>\r\n" + "			Arrival Coordinates :\r\n"
				+ "			<div></div>\r\n" + "			<input type='text' name='toCoords' value='" + toCoords
				+ "'/>Lat/Long\r\n" + "		</div>\r\n" + "		<div>\r\n"
				+ "			<input value='Save' type='submit' />\r\n"
				+ "			<input value='Clear' type='reset' />\r\n"
				+ "			<input value='' type='hidden' />\r\n"
				+ "		<input type=\"hidden\" name=\"action\" value='saveNewPlane:" + edit + ":" + flight.getId()
				+ "'/>\r\n" + "		</div>\r\n" + "	</form>\r\n" + "</body>\r\n" + "</html>\r\n";
		return page;
	}

}
