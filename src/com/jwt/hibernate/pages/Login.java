package com.jwt.hibernate.pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jwt.hibernate.controller.UserControllerServlet;

public class Login {

	public static void doLogin(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String userName = request.getParameter("username");
		String password = request.getParameter("password");

		try {
			String type = UserControllerServlet.UserDao.getUser(userName, password);
			System.out.println("here");
			if (type == null) {
				System.out.println("if");
				RequestDispatcher reponse = request.getRequestDispatcher("LoginError.html");
				reponse.forward(request, response);
				return;
			}
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();

			System.out.println("reached");

			if (type.equals("admin"))
				out.println(AdminPage.getAdminPage());
			else if (type.equals("user"))
				out.println(UserPage.getUserPage());
		} catch (Exception e) {
			System.out.println("catch");
			System.out.println(e);
		}
	}
}
