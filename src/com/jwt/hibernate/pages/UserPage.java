package com.jwt.hibernate.pages;

import java.util.List;

import com.jwt.hibernate.bean.Flight;
import com.jwt.hibernate.controller.UserControllerServlet;

public class UserPage {

	public static String getUserPage() {
		String page = "";

		page += "<!DOCTYPE html>\r\n" + "<html>\r\n" + "<head>\r\n" + "<style>\r\n" + "table, th, td {\r\n"
				+ "    border: 1px solid black;\r\n" + "    border-collapse: collapse;\r\n" + "}\r\n" + "th, td {\r\n"
				+ "    padding: 5px;\r\n" + "    text-align: left;\r\n" + "}\r\n" + "</style>\r\n" + "</head>\r\n"
				+ "<body>\r\n" + "\r\n" + "<table style=\"width:100%\">\r\n" + "  <caption>Flights</caption>\r\n"
				+ "  <tr>\r\n" + "    <th>Flight Number</th>\r\n" + "    <th>Airplane Type</th>\r\n"
				+ "    <th>From</th>\r\n" + "    <th>Departure Time</th><th>Local Time</th>\r\n" + "    <th>To</th>\r\n"
				+ "    <th>Arrival Time</th><th>Local Time</th>\r\n" + "  </tr>";

		List<Flight> flights = UserControllerServlet.FlightDao.getFlights();

		for (Flight f : flights) {
			String fromTime = null, toTime = null;

			fromTime = UserControllerServlet.Time.getWeatherAtDestination(f.getFromCoords());
			toTime = UserControllerServlet.Time.getWeatherAtDestination(f.getToCoords());

			page += "<tr>\r\n" + "    <td>" + f.getFlightNumber() + "</td>\r\n" + "    <td>" + f.getAirplaneType()
					+ "</td>\r\n" + "    <td>" + f.getFrom() + "</td>\r\n" + "    <td>" + f.getFromTime() + "</td>\r\n"
					+ "<td>" + fromTime + "</td>" + "    <td>" + f.getTo() + "</td>\r\n" + "    <td>" + f.getToTime()
					+ "</td>\r\n " + "<td>" + toTime + "</td>" + "  </tr>";
		}

		return page;

	}

}
