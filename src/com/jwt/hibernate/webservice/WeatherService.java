package com.jwt.hibernate.webservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class WeatherService {

	public WeatherService() {
	}

	public String getWeatherAtDestination(String coords) {
		String htmlFile = "";

		String[] sepCoords = coords.split("/");

		try {

			String urlString = "http://api.openweathermap.org/data/2.5/weather?lon=" + sepCoords[0] + "&lat="
					+ sepCoords[1] + "&APPID=7cd30c3189465248261ffef83ca57499&mode=xml";

			System.out.println(urlString);

			URL url = new URL(urlString);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("Accept", "application/json");

			if (con.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				htmlFile += output;
				System.out.println(htmlFile);
			}
		} catch (MalformedURLException e) {
			System.out.println("Could not form the url!!");
		} catch (IOException e) {
			System.out.println("Could not open connection!!");
		}

		String[] data = htmlFile.split("temperature value=\"");
		String temp = data[1].substring(0, 5);

		int idata = (int) ((Double.parseDouble(temp) - 273) * 10);
		double ddata = ((double) idata) / 10;

		System.out.println(ddata);

		return ddata + "�C";

	}

}